package tech.mhuang.interchan.sso.sysfunvisturl.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tech.mhuang.interchan.sso.redis.RedisCacheStartupService;
import tech.mhuang.interchan.sso.sysfunvisturl.cache.SysFunvisturlRedisOperator;
import tech.mhuang.interchan.sso.sysfunvisturl.entity.SyChanmgfunVistUrlm;
import tech.mhuang.interchan.sso.sysfunvisturl.mapper.SyChanmgfunVistUrlmMapper;
import tech.mhuang.interchan.sso.sysfunvisturl.service.ISyChanmgfunVistUrlService;
import tech.mhuang.interchan.sso.sysrole.entity.SysRole;
import tech.mhuang.interchan.sso.sysuser.entity.SysUser;
import tech.mhuang.pacebox.core.id.BaseIdeable;
import tech.mhuang.pacebox.core.util.CollectionUtil;
import tech.mhuang.pacebox.core.util.StringUtil;
import tech.mhuang.pacebox.springboot.auth.constant.AuthConstant;
import tech.mhuang.pacebox.springboot.core.service.impl.BaseServiceImpl;
import tech.mhuang.pacebox.springboot.core.spring.util.DataUtil;
import tech.mhuang.pacebox.springboot.protocol.InsertInto;
import tech.mhuang.interchan.protocol.sso.sysfunvisturl.SyChanmgfunVistUrlmAddDTO;
import tech.mhuang.interchan.protocol.sso.sysfunvisturl.SyChanmgfunVistUrlmQryDTO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ClassName: SyChanmgfunVistUrlService
 * @Description:权限可访问路径服务
 * @author: 张小虎
 * @date: 2017年7月19日 上午10:10:04
 */
@Service("syChanmgfunVistUrlService")
@Transactional(readOnly = true)
public class SyChanmgfunVistUrlService
        extends BaseServiceImpl<SyChanmgfunVistUrlm, String> implements ISyChanmgfunVistUrlService {

    @Autowired
    private BaseIdeable<String> snowflake;

    @Autowired
    private SyChanmgfunVistUrlmMapper syChanmgfunVistUrlmMapper;

    @Autowired
    private SysFunvisturlRedisOperator syChanmgfunRedisOperator;

    @Autowired
    private RedisCacheStartupService redisCacheStartupService;

    /**
     * @param funid
     * @return void
     * @Title: insertHByAuth
     * @Description: 插入历史通过权限
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void insertHByAuth(String funid, String userId, String status, String seqno) {
        InsertInto<String> into = new InsertInto<>();
        into.setId(funid);
        into.setOpDate(new Date());
        into.setUserId(userId);
        into.setReqNo(snowflake.generateId());
        into.setStatus(status);
        this.syChanmgfunVistUrlmMapper.insertIntoByAuth(into);
    }

    /**
     * @param funid
     * @return void
     * @Title: deleteByAuth
     * @Description:删除通过权限
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteByAuth(String funid) {
        this.syChanmgfunVistUrlmMapper.deleteByAuth(funid);
    }

    /**
     * <p>Title: insertPowers</p>
     * <p>Description: </p>
     *
     * @param dtos
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void insertPowersUrl(List<SyChanmgfunVistUrlmAddDTO> dtos, String userId, String seqno) {
        dtos.forEach(value -> {
            SyChanmgfunVistUrlm url = DataUtil.copyTo(value, SyChanmgfunVistUrlm.class);
            url.setOperateTime(new Date());
            url.setOperateUser(userId);
            url.setId(snowflake.generateId());
            this.syChanmgfunVistUrlmMapper.save(url);
        });
        if (CollectionUtil.isNotEmpty(dtos)) {
            InsertInto<String> into = new InsertInto<>();
            into.setReqNo(seqno);
            into.setId(dtos.get(0).getFunid());
            into.setStatus(InsertInto.ADD);
            into.setUserId(userId);
            into.setOpDate(new Date());
            this.syChanmgfunVistUrlmMapper.insertIntoByAuth(into);
        }
    }

    /**
     * <p>Title: queryFunVist</p>
     * <p>Description: </p>
     *
     * @param funid
     * @return
     */
    @Override
    public List<SyChanmgfunVistUrlmQryDTO> queryFunVist(String funid) {
        List<SyChanmgfunVistUrlm> urlm = this.syChanmgfunVistUrlmMapper.queryFunVist(funid);
        return DataUtil.copyTo(urlm, SyChanmgfunVistUrlmQryDTO.class);
    }


    /**
     * <p>Title: setVistUrlPower</p>
     * <p>Description: </p>
     *
     * @param userid
     */
    @Override
    public void setVistUrlPower(String userid) {
        List<SyChanmgfunVistUrlm> vistUrls = syChanmgfunRedisOperator.getVistUrls(userid);
        if (CollectionUtil.isEmpty(vistUrls)) {
            //不存在的时候设置权限
            setVistUrlPowerNow(userid);
        }

    }

    @Override
    public void setVistUrlPowerNow(String userId) {
        String cacheKey = AuthConstant.USER_VIST_URL_CACHEKEY;
        List<SyChanmgfunVistUrlm> urlms = this.syChanmgfunVistUrlmMapper.getUserUrlPower(userId);
        Map params = urlms.parallelStream().collect(Collectors.toMap(k -> userId.concat("-").concat(k.getUrl()), v -> v, (oldValue, newValue) -> oldValue));
        syChanmgfunRedisOperator.cacheData(cacheKey, params);
    }

    /**
     * @param roleid
     * @return void
     * @Title: setUserVistPowerByRoleAsync
     * @Description:设置人员角色权限信息
     */
    @Override
    @Async
    public void setUserVistPowerByRoleAsync(String roleid) {
        //查询角色对应的用户信息
        List<String> roleIds = new ArrayList<>();
        roleIds.add(roleid);
        List<SysUser> users = this.syChanmgfunVistUrlmMapper.queryUserByRole(roleIds);
        users.forEach(this::accept);
    }

    /**
     * <p>Title: setUserVistPowerByRolesAsync</p>
     * <p>Description: </p>
     *
     * @param roles
     */
    @Async
    @Override
    public void setUserVistPowerByRolesAsync(List<SysRole> roles) {
        if (CollectionUtil.isNotEmpty(roles)) {
            //查询角色对应的用户信息
            List<String> roleIds =
                    roles.parallelStream().map(SysRole::getRoleid).collect(Collectors.toList());
            List<SysUser> users = this.syChanmgfunVistUrlmMapper.queryUserByRole(roleIds);
            users.forEach(this::accept);
        }
    }

    /**
     * <p>Title: deleteByFunsIds</p>
     * <p>Description: </p>
     *
     * @param ids
     * @param userId
     * @param reqNo
     * @see ISyChanmgfunVistUrlService#deleteByFunsIds(List, String, String, String)
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void deleteByFunsIds(List<String> ids, String userId, String status, String reqNo) {
        InsertInto<List<String>> into = new InsertInto<>();
        into.setId(ids);
        into.setOpDate(new Date());
        into.setUserId(userId);
        into.setReqNo(reqNo);
        into.setStatus(status);
        this.syChanmgfunVistUrlmMapper.insertIntoByAuths(into);
        this.syChanmgfunVistUrlmMapper.deleteByAuths(ids);
    }

    /**
     * <p>Title: reloadVistUrl</p>
     * <p>Description: </p>
     *
     * @param userId
     * @see ISyChanmgfunVistUrlService#reloadVistUrl(String)
     */
    @Override
    public void reloadVistUrl(String userId) {
        if (StringUtil.isNotBlank(userId)) {
            this.setVistUrlPowerNow(userId);
        } else {
            redisCacheStartupService.execute();
        }

    }

    private void accept(SysUser user) {
        setVistUrlPowerNow(user.getUserid());
    }
}
