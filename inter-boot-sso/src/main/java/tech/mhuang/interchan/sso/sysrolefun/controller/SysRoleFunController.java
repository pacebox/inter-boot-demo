
package tech.mhuang.interchan.sso.sysrolefun.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tech.mhuang.interchan.sso.sysrolefun.service.ISysRoleFunService;
import tech.mhuang.pacebox.core.util.CollectionUtil;
import tech.mhuang.pacebox.springboot.core.controller.BaseController;
import tech.mhuang.pacebox.springboot.core.local.GlobalHeaderThreadLocal;
import tech.mhuang.pacebox.springboot.core.spring.util.DataUtil;
import tech.mhuang.pacebox.springboot.protocol.GlobalHeader;
import tech.mhuang.pacebox.springboot.protocol.Result;
import tech.mhuang.interchan.protocol.sso.sysfunrole.SysRoleFunAddDTO;
import tech.mhuang.interchan.protocol.sso.sysfunrole.SysRoleFunDTO;
import tech.mhuang.interchan.protocol.sso.sysfunrole.SysRoleFunTreeDTO;
import tech.mhuang.interchan.protocol.sso.sysfunrole.SysRoleFunTreeVO;
import tech.mhuang.interchan.protocol.sso.sysfunrole.SysRoleFunVO;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @ClassName: SysRoleFunController
 * @Description:系统角色功能
 * @author: admin
 * @date: 2017年7月20日 下午6:48:48
 */
@RestController
@Tag(name = "系统角色功能管理")
@RequestMapping("/sy/sysrolefun")
public class SysRoleFunController extends BaseController {

    @Autowired
    private ISysRoleFunService sysRoleFunService;

    @Operation(summary = "保存角色功能信息")
    @PostMapping(value = "/setRoleFun")
    public Result<?> setRoleFun(@RequestBody SysRoleFunAddDTO sysRoleFunAddDTO) {
        GlobalHeader globalHeader = GlobalHeaderThreadLocal.getOrException();
        sysRoleFunService.saveRoleFun(sysRoleFunAddDTO, globalHeader.getUserId());
        return Result.ok();
    }


    @Operation(summary = "树形结构查询角色权限信息")
    @GetMapping(value = "/queryRoleFunTree")
    public Result<List<SysRoleFunTreeVO>> queryRoleFunTree(@RequestParam(required = false) String roleid) {
        GlobalHeaderThreadLocal.getOrException();
        List<SysRoleFunTreeDTO> roleFunVos = this.sysRoleFunService.queryRoleFunTree(roleid);
        List<SysRoleFunTreeVO> treeVos = this.structFunTree(roleFunVos);
        return Result.ok(treeVos);
    }


    /**
     * @param roleFunVos
     * @return List<SysRoleFunTreeVo>
     * @Title: structFunTree
     * @Description: 组装功能树
     */
    private List<SysRoleFunTreeVO> structFunTree(List<SysRoleFunTreeDTO> roleFunVos) {
        List<SysRoleFunTreeVO> vos = new CopyOnWriteArrayList<>();
        if (CollectionUtil.isNotEmpty(roleFunVos)) {
            roleFunVos.parallelStream().forEach((data) -> {
                if ("TOP".equals(data.getParentid())) {
                    SysRoleFunTreeVO vo = DataUtil.copyTo(data, SysRoleFunTreeVO.class);
                    vos.add(vo);
                    List<SysRoleFunTreeVO> children = this.getRoleFunChildren(data, roleFunVos);
                    if (CollectionUtil.isNotEmpty(children)) {
                        children.sort(Comparator.comparingInt(SysRoleFunTreeVO::getOrderval));
                    }
                    vo.setChildren(children);
                }
            });
        }
        if (CollectionUtil.isNotEmpty(vos)) {
            vos.sort((data1, data2) -> {
                try {
                    return Integer.compare(data1.getOrderval(), data2.getOrderval());
                } catch (Exception e) {
                    return 0;
                }
            });
        }
        return vos;
    }


    /**
     * @param current
     * @param roleFunVos
     * @return List<SysRoleFunTreeVo>
     * @Title: getRoleFunChildren
     * @Description: 获取当前角色权限的子
     */
    private List<SysRoleFunTreeVO> getRoleFunChildren(SysRoleFunTreeDTO current, List<SysRoleFunTreeDTO> roleFunVos) {
        List<SysRoleFunTreeVO> vos = new CopyOnWriteArrayList<>();
        roleFunVos.parallelStream().forEach((data) -> {
            if (current.getFunid().equals(data.getParentid())) {
                SysRoleFunTreeVO vo = DataUtil.copyTo(data, SysRoleFunTreeVO.class);
                vos.add(vo);
                vo.setChildren(this.getRoleFunChildren(data, roleFunVos));
            }
        });
        return vos;
    }


    @Operation(summary = "查询角色功能信息")
    @GetMapping(value = "/queryRoleFun")
    public Result<List<SysRoleFunVO>> queryRoleFun(@RequestParam String roleid) {
        GlobalHeaderThreadLocal.getOrException();
        List<SysRoleFunDTO> dtos = this.sysRoleFunService.queryRoleFun(roleid);
        List<SysRoleFunVO> roleFunVos = DataUtil.copyTo(dtos, SysRoleFunVO.class);
        return Result.ok(roleFunVos);
    }

}
