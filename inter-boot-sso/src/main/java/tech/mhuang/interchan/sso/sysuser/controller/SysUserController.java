package tech.mhuang.interchan.sso.sysuser.controller;

import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tech.mhuang.interchan.sso.sysuser.entity.SysUser;
import tech.mhuang.interchan.sso.sysuser.service.ISysUserService;
import tech.mhuang.pacebox.jwt.admin.external.IJwtProducer;
import tech.mhuang.pacebox.springboot.core.controller.BaseController;
import tech.mhuang.pacebox.springboot.core.local.GlobalHeaderThreadLocal;
import tech.mhuang.pacebox.springboot.core.protocol.page.PageUtil;
import tech.mhuang.pacebox.springboot.core.spring.util.DataUtil;
import tech.mhuang.pacebox.springboot.protocol.GlobalHeader;
import tech.mhuang.pacebox.springboot.protocol.Result;
import tech.mhuang.pacebox.springboot.protocol.data.PageVO;
import tech.mhuang.interchan.protocol.sso.UserDTO;
import tech.mhuang.interchan.protocol.sso.sysuser.LoginSysUserDTO;
import tech.mhuang.interchan.protocol.sso.sysuser.LoginSysUserVO;
import tech.mhuang.interchan.protocol.sso.sysuser.SysUserAddDTO;
import tech.mhuang.interchan.protocol.sso.sysuser.SysUserDTO;
import tech.mhuang.interchan.protocol.sso.sysuser.SysUserUpdateDTO;
import tech.mhuang.interchan.protocol.sso.sysuser.SysUserVO;
import tech.mhuang.interchan.protocol.sso.sysuser.dto.SysUserPageDTO;
import tech.mhuang.interchan.protocol.sso.sysuser.dto.SysUserPageQryDTO;
import tech.mhuang.interchan.protocol.sso.sysuser.pwd.ResetPwdDTO;
import tech.mhuang.interchan.protocol.sso.sysuser.pwd.UpdateCenterPwdDTO;
import tech.mhuang.interchan.protocol.sso.sysuser.pwd.UpdatePwdDTO;
import tech.mhuang.interchan.protocol.sso.sysuser.request.SysUserPageQryQVO;
import tech.mhuang.interchan.protocol.sso.sysuser.response.SysUserPageQryRVO;
import tech.mhuang.interchan.protocol.sso.sysuser.response.SysUserRVO;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: SysUserController
 * @Description:系统用户管理
 * @author: mhuang
 * @date: 2017年7月13日 下午3:39:48
 */
@RestController("sy/sysuser")
@RequestMapping("sy/sysuser")
@Tag(name = "系统用户管理")
public class SysUserController extends BaseController {

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    @Lazy
    private IJwtProducer webJwt;

    @GetMapping("/queryUserByPage")
    @Operation(summary = "系统用户分页查询")
    @Parameters({
            @Parameter(in = ParameterIn.QUERY, name = "username", description  = "用户名"),
            @Parameter(in = ParameterIn.QUERY, name = "mobilephone", description  = "移动电话"),
            @Parameter(in = ParameterIn.QUERY, name = "rows", description  = "条数", required = true),
            @Parameter(in = ParameterIn.QUERY, name = "start", description  = "页数", required = true)
    })
    public Result<PageVO<SysUserVO>> queryUserByPage(@RequestParam(required = false) String username,
                                                     @RequestParam(required = false) String mobilephone, @RequestParam("start") Integer start,
                                                     @RequestParam("rows") Integer rows) {
        SysUserDTO sysUserDTO = new SysUserDTO();
        //组装对象
        packPageSysUserDTO(username, mobilephone, start, rows, sysUserDTO);

        return Result.ok(sysUserService.queryUserByPage(sysUserDTO));
    }


    @SuppressWarnings("unchecked")
    @GetMapping("/pageForOrder")
    @Operation(summary = "系统用户分页查询")
    public Result<PageVO<SysUserPageQryRVO>> pageForOrder(@ModelAttribute SysUserPageQryQVO qryVo) {
        SysUserPageQryDTO sysUserDTO = DataUtil.copyTo(qryVo, SysUserPageQryDTO.class);
        SysUser sysUser = DataUtil.copyTo(qryVo, SysUser.class);
        int count = this.sysUserService.count(sysUser);
        PageVO<SysUserPageQryRVO> pageVo = new PageVO<>();
        pageVo.setTotalSize(count);
        if (PageUtil.checkPage(count,qryVo)) {
            List<SysUserPageDTO> pageDtos = sysUserService.pageForOrder(sysUserDTO);
            pageVo.setResult(DataUtil.copyTo(pageDtos, SysUserPageQryRVO.class));
        }
        return Result.ok(pageVo);
    }


    @Operation(summary = "新增系统用户")
    @PostMapping("/saveUser")
    public Result<?> saveUser(@RequestBody SysUserAddDTO sysUserAddDto) {
        GlobalHeader globalHeader = GlobalHeaderThreadLocal.getOrException();
        sysUserAddDto.setOperateUser(globalHeader.getUserId());
        sysUserService.saveUser(sysUserAddDto);
        return Result.ok();
    }

    @Operation(summary = "修改系统用户")
    @PutMapping("/updateUser")
    public Result<?> updateUser(@RequestBody SysUserUpdateDTO sysUserUpdateDTO) {
        GlobalHeader globalHeader = GlobalHeaderThreadLocal.getOrException();
        sysUserUpdateDTO.setOperateUser(globalHeader.getUserId());
        sysUserService.updateUser(sysUserUpdateDTO);
        return Result.ok();
    }

    @Operation(summary = "修改密码")
    @PutMapping("/updatePassword")
    public Result<?> updatePassword(@RequestBody UpdatePwdDTO updatePwd) {
        GlobalHeader globalHeader = GlobalHeaderThreadLocal.getOrException();
        updatePwd.setUserid(globalHeader.getUserId());
        sysUserService.updatePwd(updatePwd);
        return Result.ok();
    }

    @Operation(summary = "系统登录密码修改")
    @PutMapping("/updateLoginPwd")
    public Result<?> updateLoginPwd(@RequestBody UpdateCenterPwdDTO updatePwd) {

        GlobalHeader globalHeader = GlobalHeaderThreadLocal.getOrException();
        updatePwd.setUserid(globalHeader.getUserId());
        sysUserService.updateLoginPwd(updatePwd);
        return Result.ok();
    }

    @Operation(summary = "重置密码")
    @PutMapping("/resetPassword")
    public Result<?> resetPassword(@RequestBody ResetPwdDTO resetPwd) {
        GlobalHeader globalHeader = GlobalHeaderThreadLocal.getOrException();
        resetPwd.setOperateUser(globalHeader.getUserId());
        sysUserService.resetPwd(resetPwd);
        return Result.ok();
    }

    @Operation(summary = "用户锁定/解锁")
    @PutMapping("/lockUser")
    public Result<?> lockUser(@RequestBody UserDTO lockUserDTO) {
        lockUserDTO.setOperateUser(GlobalHeaderThreadLocal.getOrException().getUserId());
        sysUserService.lockUser(lockUserDTO);
        return Result.ok();
    }

    /**
     * @return void
     * @Title: packPageSysUserDTO
     * @Description: 组装user对象
     */
    private void packPageSysUserDTO(String username, String mobilePhone, Integer start, Integer rows, SysUserDTO sysUserDTO) {
        sysUserDTO.setMobilephone(mobilePhone);
        sysUserDTO.setStart(start);
        sysUserDTO.setUsername(username);
        sysUserDTO.setRows(rows);
    }

    @Operation(summary = "系统用户使用密码登录")
    @GetMapping("/loginUsePwd")
    public Result<?> loginUsePwd(@RequestParam String mobilephone, @RequestParam String password) {

        LoginSysUserDTO userDto = this.sysUserService.loginUsePwd(mobilephone, password);
        LoginSysUserVO userVo = DataUtil.copyTo(userDto, LoginSysUserVO.class);
        Map<String, Object> claimMap = new HashMap<>(4);
        claimMap.put("userId", userDto.getUserid());
        claimMap.put("type", "webuser");
        String token = webJwt.create(claimMap);
        userVo.setToken(token);
        userVo.setAuthType("webJwt");
        return Result.ok(userVo);
    }

    @SuppressWarnings("unchecked")
    @GetMapping("/findByUserIds")
    @Operation(summary = "查询用户信息")
    public Result<List<SysUserRVO>> findByUserIds(@RequestParam String userIds) {
        GlobalHeaderThreadLocal.getOrException();
        List<SysUser> users = sysUserService.findByUserIds(Arrays.asList(userIds.split(",")));
        return Result.ok(DataUtil.copyTo(users, SysUserRVO.class));
    }
}