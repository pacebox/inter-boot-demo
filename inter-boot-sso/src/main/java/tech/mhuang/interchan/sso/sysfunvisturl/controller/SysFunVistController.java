
package tech.mhuang.interchan.sso.sysfunvisturl.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tech.mhuang.interchan.sso.sysfunvisturl.service.ISyChanmgfunVistUrlService;
import tech.mhuang.pacebox.springboot.core.controller.BaseController;
import tech.mhuang.pacebox.springboot.core.local.GlobalHeaderThreadLocal;
import tech.mhuang.pacebox.springboot.core.spring.util.DataUtil;
import tech.mhuang.pacebox.springboot.protocol.GlobalHeader;
import tech.mhuang.pacebox.springboot.protocol.Result;
import tech.mhuang.interchan.protocol.sso.sysfunvisturl.SyChanmgfunVistUrlmQryDTO;
import tech.mhuang.interchan.protocol.sso.sysfunvisturl.SyChanmgfunVistUrlmQryVO;

import java.util.List;

/**
 * @ClassName: SysFunController
 * @Description:系统功能权限
 * @author: admin
 * @date: 2017年7月20日 下午6:48:48
 */
@RestController
@Tag(name = "系统功能权限访问地址管理")
@RequestMapping("/sy/sysfunvist")
public class SysFunVistController extends BaseController {

    @Autowired
    private ISyChanmgfunVistUrlService syChanmgfunVistUrlService;

    @SuppressWarnings("unchecked")
    @Operation(summary = "通过功能权限查询访问vo")
    @GetMapping(value = "/getVistUrl")
    public Result<List<SyChanmgfunVistUrlmQryVO>> getVistUrl(@RequestParam String funid) {
        GlobalHeaderThreadLocal.getOrException();
        List<SyChanmgfunVistUrlmQryDTO> dtos = this.syChanmgfunVistUrlService.queryFunVist(funid);
        List<SyChanmgfunVistUrlmQryVO> vos = DataUtil.copyTo(dtos, SyChanmgfunVistUrlmQryVO.class);
        return Result.ok(vos);
    }


    @Operation(summary = "重新加载可访问地址等")
    @GetMapping(value = "/reloadVistUrl")
    public Result<?> reloadVistUrl() {
        GlobalHeader userHead = GlobalHeaderThreadLocal.get();
        if (userHead != null) {
            this.syChanmgfunVistUrlService.reloadVistUrl(userHead.getUserId());
        } else {
            this.syChanmgfunVistUrlService.reloadVistUrl(null);
        }

        return Result.ok();
    }
}
