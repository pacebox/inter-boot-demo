
package tech.mhuang.interchan.sso.sysfun.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tech.mhuang.interchan.sso.sysfun.service.ISysFunService;
import tech.mhuang.pacebox.core.util.StringUtil;
import tech.mhuang.pacebox.springboot.core.controller.BaseController;
import tech.mhuang.pacebox.springboot.core.local.GlobalHeaderThreadLocal;
import tech.mhuang.pacebox.springboot.core.spring.util.DataUtil;
import tech.mhuang.pacebox.springboot.protocol.Result;
import tech.mhuang.pacebox.springboot.protocol.data.PageVO;
import tech.mhuang.interchan.protocol.sso.sysfun.SysFunAddDTO;
import tech.mhuang.interchan.protocol.sso.sysfun.SysFunModDTO;
import tech.mhuang.interchan.protocol.sso.sysfun.SysFunPageQueryDTO;
import tech.mhuang.interchan.protocol.sso.sysfun.SysFunQueryDTO;
import tech.mhuang.interchan.protocol.sso.sysfun.SysFunTreeQueryDTO;
import tech.mhuang.interchan.protocol.sso.sysfun.SysFunVO;

import java.util.List;

/**
 * 权限控制器
 *
 * @author zhangxh
 * @since 1.0.0
 */
@RestController
@Tag(name = "系统功能权限管理")
@RequestMapping("/sy/sysfun")
public class SysFunController extends BaseController {

    @Autowired
    private ISysFunService sysFunService;

    @Operation(summary = "新增功能权限信息")
    @PostMapping(value = "/saveFun")
    public Result<?> saveFun(@RequestBody SysFunAddDTO sysFunAddDTO) {
        sysFunService.saveFun(sysFunAddDTO, GlobalHeaderThreadLocal.getOrException().getUserId());
        return Result.ok();
    }

    @Operation(summary = "修改功能权限信息")
    @PutMapping(value = "/updateFun")
    public Result<?> updateFun(@RequestBody SysFunModDTO sysFunModDTO) {
        sysFunService.updateFun(sysFunModDTO, GlobalHeaderThreadLocal.getOrException().getUserId());
        return Result.ok();
    }


    @Operation(summary = "分页查询功能权限信息")
    @GetMapping(value = "/queryFunByPage")
    public Result<PageVO<SysFunVO>> queryFunByPage(@ModelAttribute SysFunPageQueryDTO pageQry) {
        GlobalHeaderThreadLocal.getOrException();
        PageVO<SysFunVO> pageVo = this.sysFunService.queryFunByPage(pageQry);
        return Result.ok(pageVo);
    }

    @Operation(summary = "树形结构查询权限信息")
    @GetMapping(value = "/queryFunTree")
    public Result<PageVO<SysFunVO>> queryFunTree(@ModelAttribute SysFunTreeQueryDTO queryTree) {
        GlobalHeaderThreadLocal.getOrException();
        if (StringUtil.isNotBlank(queryTree.getNodeid())) {
            queryTree.setParentid(queryTree.getNodeid());
        } else {
            queryTree.setParentid(queryTree.getParentid());
        }
        List<SysFunVO> funVos = this.sysFunService.queryFunTree(queryTree);
        funVos.parallelStream().forEach(t -> {
            if (t.isHasChild()) {
                t.setExpand(false);
                t.setLeaf(false);
            } else {
                t.setExpand(true);
                t.setLeaf(true);
            }
        });
        PageVO pageVo = new PageVO();
        pageVo.setResult(funVos);
        pageVo.setTotalSize(funVos.size());
        return Result.ok(pageVo);
    }

    @Operation(summary = "删除功能权限信息")
    @DeleteMapping(value = "/deleteFun")
    public Result<?> remove(@RequestParam String funid) {
        this.sysFunService.deleteFun(funid, GlobalHeaderThreadLocal.getOrException().getUserId());
        return Result.ok();
    }

    @Operation(summary = "查询功能权限信息")
    @GetMapping(value = "/queryFun")
    public Result<SysFunVO> queryFun(@RequestParam String funid) {
        SysFunQueryDTO dto = this.sysFunService.queryFun(funid, true);
        SysFunVO funVo = DataUtil.copyTo(dto, SysFunVO.class);
        return Result.ok(funVo);
    }
}
