package tech.mhuang.interchan.sso.sysrole.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tech.mhuang.interchan.sso.sysrole.entity.SysRole;
import tech.mhuang.interchan.sso.sysrole.service.ISysRoleService;
import tech.mhuang.pacebox.springboot.core.controller.BaseController;
import tech.mhuang.pacebox.springboot.core.local.GlobalHeaderThreadLocal;
import tech.mhuang.pacebox.springboot.core.spring.util.DataUtil;
import tech.mhuang.pacebox.springboot.protocol.GlobalHeader;
import tech.mhuang.pacebox.springboot.protocol.Result;
import tech.mhuang.pacebox.springboot.protocol.data.Page;
import tech.mhuang.pacebox.springboot.protocol.data.PageVO;
import tech.mhuang.interchan.protocol.sso.sysrole.SysRoleAddDTO;
import tech.mhuang.interchan.protocol.sso.sysrole.SysRoleModDTO;
import tech.mhuang.interchan.protocol.sso.sysrole.SysRolePageQueryDTO;
import tech.mhuang.interchan.protocol.sso.sysrole.SysRoleQueryDTO;
import tech.mhuang.interchan.protocol.sso.sysrole.SysRoleVO;
import tech.mhuang.interchan.protocol.sso.sysrole.request.SysRolePageQueryQVO;
import tech.mhuang.interchan.protocol.sso.sysrole.response.SysRoleRVO;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName: SysRoleController
 * @Description:系统角色
 * @author: admin
 * @date: 2017年7月20日 下午6:48:48
 */
@RestController
@Tag(name = "系统角色管理")
@RequestMapping("/sy/sysrole")
public class SysRoleController extends BaseController {

    @Autowired
    private ISysRoleService sysRoleService;

    @Operation(summary = "新增角色信息")
    @PostMapping(value = "/saveRole")
    public Result<?> saveRole(@RequestBody SysRoleAddDTO sysRoleAddDTO) {
        GlobalHeader globalHeader = GlobalHeaderThreadLocal.getOrException();
        sysRoleService.saveRole(sysRoleAddDTO, globalHeader.getUserId());
        return Result.ok();
    }

    @Operation(summary = "修改角色信息")
    @PutMapping(value = "/updateRole")
    public Result<?> updateRole(@RequestBody SysRoleModDTO sysRoleModDTO) {
        GlobalHeader globalHeader = GlobalHeaderThreadLocal.getOrException();
        sysRoleService.updateRole(sysRoleModDTO, globalHeader.getUserId());
        return Result.ok();
    }


    @Operation(summary = "分页查询角色信息")
    @GetMapping(value = "/queryRoleByPage")
    public Result<PageVO<SysRoleVO>> queryRoleByPage(@ModelAttribute SysRolePageQueryDTO dto) {
        GlobalHeaderThreadLocal.getOrException();
        PageVO<SysRoleVO> pageVo = this.sysRoleService.queryRoleByPage(dto);
        return Result.ok(pageVo);
    }

    @Operation(summary = "删除角色信息")
    @DeleteMapping(value = "/deleteRole")
    public Result<?> remove(@RequestParam String roleid) {
        GlobalHeader globalHeader = GlobalHeaderThreadLocal.getOrException();
        this.sysRoleService.deleteRole(roleid, globalHeader.getUserId());
        return Result.ok();
    }

    @Operation(summary = "查询角色信息")
    @GetMapping(value = "/query")
    public Result<SysRoleVO> queryRole(@RequestParam String roleid) {
        GlobalHeaderThreadLocal.getOrException();
        SysRoleQueryDTO dto = this.sysRoleService.queryRole(roleid, true);
        SysRoleVO roleVo = DataUtil.copyTo(dto, SysRoleVO.class);
        return Result.ok(roleVo);
    }

    @Operation(summary = "分页查询角色信息")
    @GetMapping(value = "/pageOrderRole")
    public Result<PageVO<SysRoleRVO>> pageOrderRole(@ModelAttribute SysRolePageQueryQVO dto) {
        GlobalHeaderThreadLocal.getOrException();
        PageVO<SysRoleRVO> pageVo = new PageVO();
        Page page = new Page();
        page.setRecord(DataUtil.copyTo(dto, SysRole.class));
        int count = this.sysRoleService.pageCount(page);
        if (count > 0) {
            List<SysRole> sysRoles = this.sysRoleService.pageOrderRole(dto);
            pageVo.setResult(DataUtil.copyTo(sysRoles, SysRoleRVO.class));
        }
        return Result.ok(pageVo);
    }

    @Operation(summary = "查询角色信息")
    @GetMapping(value = "/findByRoleIds")
    public Result<List<SysRoleRVO>> findByRoleIds(String roleIds) {
        GlobalHeaderThreadLocal.getOrException();
        List<SysRole> sysRoles = this.sysRoleService.findByRoleIds(Arrays.asList(roleIds.split(",")));
        return Result.ok(DataUtil.copyTo(sysRoles, SysRoleRVO.class));
    }
}
