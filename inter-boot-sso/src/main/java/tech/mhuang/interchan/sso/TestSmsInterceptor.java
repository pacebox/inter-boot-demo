package tech.mhuang.interchan.sso;

import org.springframework.stereotype.Component;
import tech.mhuang.pacebox.core.chain.BaseChain;
import tech.mhuang.pacebox.sms.domain.SmsSendRequest;
import tech.mhuang.pacebox.sms.domain.SmsSendResult;
import tech.mhuang.pacebox.sms.inteceptor.SmsSendInterceptor;

@Component
public class TestSmsInterceptor implements SmsSendInterceptor {

    @Override
    public SmsSendResult interceptor(BaseChain<SmsSendRequest, SmsSendResult> chain) {
        System.out.println("发送得内容是:" + chain.request());
        System.out.println("获取到得内容是:" + chain.proceed(chain.request()));
        System.out.println("返回空对象");
        return new SmsSendResult();
    }
}
