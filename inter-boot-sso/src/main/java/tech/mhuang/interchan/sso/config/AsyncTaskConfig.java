package tech.mhuang.interchan.sso.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.mhuang.pacebox.core.async.AsyncTaskService;
import tech.mhuang.pacebox.core.async.DefaultAsyncTaskSupport;

/**
 * @ClassName: TaskConfig
 * @Author: Ever
 * @Description: 异步线程池
 * @Date: 2019/12/31 9:02
 * @Version: 1.0
 */
@Configuration
public class AsyncTaskConfig {

    @Bean
    public AsyncTaskService buildAsync() {
        return new DefaultAsyncTaskSupport();
    }
}