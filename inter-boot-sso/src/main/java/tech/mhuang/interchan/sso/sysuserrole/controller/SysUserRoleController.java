package tech.mhuang.interchan.sso.sysuserrole.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tech.mhuang.interchan.protocol.sso.sysuserrole.SysUserFunDTO;
import tech.mhuang.interchan.protocol.sso.sysuserrole.SysUserFunVO;
import tech.mhuang.interchan.protocol.sso.sysuserrole.SysUserRoleAddDTO;
import tech.mhuang.interchan.protocol.sso.sysuserrole.SysUserRoleCheckDTO;
import tech.mhuang.interchan.protocol.sso.sysuserrole.SysUserRoleCheckVO;
import tech.mhuang.interchan.protocol.sso.sysuserrole.SysUserRoleDTO;
import tech.mhuang.interchan.protocol.sso.sysuserrole.SysUserRoleVO;
import tech.mhuang.interchan.sso.sysfunvisturl.service.ISyChanmgfunVistUrlService;
import tech.mhuang.interchan.sso.sysuserrole.service.ISysUserRoleService;
import tech.mhuang.pacebox.springboot.core.controller.BaseController;
import tech.mhuang.pacebox.springboot.core.local.GlobalHeaderThreadLocal;
import tech.mhuang.pacebox.springboot.core.spring.util.DataUtil;
import tech.mhuang.pacebox.springboot.protocol.GlobalHeader;
import tech.mhuang.pacebox.springboot.protocol.Result;
import tech.mhuang.pacebox.springboot.protocol.data.PageVO;

import java.util.List;

/**
 * @ClassName: SysUserRoleController
 * @Description:系统用户角色
 * @author: admin
 * @date: 2017年7月20日 下午6:48:48
 */
@RestController
@Tag(name = "系统用户角色管理")
@RequestMapping("/sy/sysuserrole")
public class SysUserRoleController extends BaseController {


    @Autowired
    private ISyChanmgfunVistUrlService syChanmgfunVistUrlService;

    @Autowired
    private ISysUserRoleService sysUserRoleService;

    @Operation(summary = "保存用户角色信息")
    @PostMapping(value = "/setRoleUser")
    public Result<?> setRoleUser(@RequestBody SysUserRoleAddDTO sysUserRoleAddDTO) {
        GlobalHeader globalHeader = GlobalHeaderThreadLocal.getOrException();
        sysUserRoleService.saveUserRole(sysUserRoleAddDTO, globalHeader.getUserId());
        return Result.ok();
    }


    @Operation(summary = "查询用户角色信息")
    @GetMapping(value = "/queryUserRole")
    public Result<List<SysUserRoleVO>> queryUserRole(@RequestParam(required = false) String userid) {
        GlobalHeaderThreadLocal.getOrException();
        List<SysUserRoleDTO> userRoles = this.sysUserRoleService.queryUserRole(userid);
        List<SysUserRoleVO> roleVos = DataUtil.copyTo(userRoles, SysUserRoleVO.class);
        return Result.ok(roleVos);
    }


    @Operation(summary = "查询用户角色选择信息")
    @GetMapping(value = "/queryUserRoleCheck")
    public Result<PageVO<SysUserRoleCheckVO>> queryUserRoleAll(@RequestParam(required = false) String userid) {
        GlobalHeaderThreadLocal.getOrException();
        List<SysUserRoleCheckDTO> userRoles = this.sysUserRoleService.queryUserRoleCheck(userid);
        List<SysUserRoleCheckVO> roleVos = DataUtil.copyTo(userRoles, SysUserRoleCheckVO.class);
        PageVO<SysUserRoleCheckVO> pageVO = new PageVO();
        pageVO.setResult(DataUtil.copyTo(roleVos,SysUserRoleCheckVO.class));
        pageVO.setTotalSize(roleVos.size());
        return Result.ok(pageVO);
    }


    @Operation(summary = "查询用户权限")
    @GetMapping(value = "/queryUserFun")
    public Result<List<SysUserFunVO>> queryUserFun(@RequestParam String userid) {
        GlobalHeaderThreadLocal.getOrException();
        List<SysUserFunDTO> dtos = this.sysUserRoleService.queryUserFun(userid);
        List<SysUserFunVO> roleFunVos = DataUtil.copyTo(dtos, SysUserFunVO.class);
        return Result.ok(roleFunVos);
    }


    @Operation(summary = "查询用户权限")
    @GetMapping(value = "/queryUserFunForLogin")
    public Result<List<SysUserFunVO>> queryUserFunForLogin() {
        GlobalHeader header = GlobalHeaderThreadLocal.getOrException();
        List<SysUserFunDTO> dtos = this.sysUserRoleService.queryUserFun(header.getUserId());
        List<SysUserFunVO> roleFunVos = DataUtil.copyTo(dtos, SysUserFunVO.class);
        this.syChanmgfunVistUrlService.setVistUrlPowerNow(header.getUserId());
        this.sysUserRoleService.setUserRoleToCache(header.getUserId());
        return Result.ok(roleFunVos);
    }
}
