package tech.mhuang.interchan.sso.redis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.mhuang.interchan.sso.sysfunvisturl.cache.SysFunvisturlRedisOperator;
import tech.mhuang.interchan.sso.sysfunvisturl.entity.SyChanmgfunExcludeUrl;
import tech.mhuang.interchan.sso.sysfunvisturl.mapper.SyChanmgfunVistUrlmMapper;
import tech.mhuang.pacebox.core.async.AsyncTask;
import tech.mhuang.pacebox.core.async.AsyncTaskService;
import tech.mhuang.pacebox.core.util.CollectionUtil;
import tech.mhuang.pacebox.springboot.auth.constant.AuthConstant;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ClassName: redisCacheStartupService
 * @Author: Ever
 * @Description: redis 异步加载
 * @Date: 2019/12/30 13:15
 * @Version: 1.0
 */
@Service
@Slf4j
public class RedisCacheStartupService implements AsyncTask {
    /**
     * 成功
     */
    private static final String SUCCESS = "success";

    @Autowired
    AsyncTaskService asyncTaskService;

    @Autowired
    private SyChanmgfunVistUrlmMapper syChanmgfunVistUrlmMapper;

    @Autowired
    private SysFunvisturlRedisOperator syChanmgfunRedisOperator;


    @Override
    public void onSuccess(Object result) {
        log.info("redis异步加载:{}", result);
    }

    @Override
    public void onFailed(Throwable t) {
        log.error("redis异步加载:{}", t.getMessage(), t);
    }

    @Override
    public String execute() {
        //查询数据库，并放入缓存中
        List<SyChanmgfunExcludeUrl> urls = syChanmgfunVistUrlmMapper.getExcludeUrl();
        if (CollectionUtil.isNotEmpty(urls)) {
            Map datas =
                    urls.parallelStream().collect(Collectors.groupingBy(SyChanmgfunExcludeUrl::getType));
            syChanmgfunRedisOperator.cacheData(AuthConstant.AUTH_DICT_KEY, datas);
        }
        return SUCCESS;
    }

}
