package tech.mhuang.interchan.sso;

import io.opentracing.Tracer;
import lombok.Data;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.mhuang.interchan.sso.sysuser.service.ISysUserService;
import tech.mhuang.pacebox.core.dict.BasicDict;
import tech.mhuang.pacebox.core.file.FileUtil;
import tech.mhuang.pacebox.oss.OssTemplate;
import tech.mhuang.pacebox.oss.domain.OssDeleteRequest;
import tech.mhuang.pacebox.oss.domain.OssDeleteResult;
import tech.mhuang.pacebox.oss.domain.OssDownloadRequest;
import tech.mhuang.pacebox.oss.domain.OssDownloadResult;
import tech.mhuang.pacebox.oss.domain.OssStorageType;
import tech.mhuang.pacebox.oss.domain.OssUploadRequest;
import tech.mhuang.pacebox.oss.domain.OssUploadResult;
import tech.mhuang.pacebox.sms.SmsTemplate;
import tech.mhuang.pacebox.sms.domain.SmsSendRequest;
import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.annation.DataSecureField;
import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.annation.DecryptMapping;
import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.annation.EncryptMapping;
import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.consts.DecryptType;
import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.consts.EncryptType;
import tech.mhuang.pacebox.springboot.core.entity.RequestModel;
import tech.mhuang.pacebox.springboot.core.local.GlobalHeaderThreadLocal;
import tech.mhuang.pacebox.springboot.core.permission.annotation.Permission;
import tech.mhuang.pacebox.springboot.core.rest.SingleRestTemplate;
import tech.mhuang.pacebox.springboot.protocol.Result;

import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private  ISysUserService sysUserService;
    @Autowired
    private  SingleRestTemplate singleRestTemplate;
    @Autowired(required = false)
    private  SmsTemplate smsTemplate;
    @Autowired(required = false)
    private  OssTemplate ossTemplate;


    @Permission(code = "ceshi")
    @GetMapping("/permission")
    public Result permission(String id){
        return Result.ok(GlobalHeaderThreadLocal.get().getExtraMap().get("code"));
    }
    @Permission(code = "ceshi2")
    @GetMapping("/permission2")
    public Result permission2(String id){
        return Result.ok(GlobalHeaderThreadLocal.get().getExtraMap().get("a"));
    }
    @GetMapping("/permissionTest")
    public Result permissionTest(){
       return Result.ok();
    }
    @GetMapping("/sendSMS")
    public Result sendSMS() {
        SmsSendRequest smsSendRequest = new SmsSendRequest();
        smsSendRequest.setType("aliyun"); //对应配置的key
        smsSendRequest.setMobile("");//发送的短信手机号
        smsSendRequest.setSign("");//签名
        smsSendRequest.setTemplateCode("");//模板号
        smsSendRequest.setTemplateParam(new BasicDict().set("code", "1234"));//替换的模板内容
        smsSendRequest.setAppId("");//应用id
        smsSendRequest.setExtendParam(null);//发送后扩展字段
        smsSendRequest.setContent("");//短信内容
        return Result.ok(smsTemplate.send(smsSendRequest));
    }

    @GetMapping("/localUpload")
    public Result localUpload() throws IOException {
        OssUploadRequest ossUploadRequest = new OssUploadRequest();
        ossUploadRequest.setType("local");//文件上传
        ossUploadRequest.setStorageType(OssStorageType.LOCAL);//上传本地
        ossUploadRequest.setBucketName("d:\\");//本地得路径
        ossUploadRequest.setKey("test.png"); //生成得文件名
        ossUploadRequest.setLocalUrl("d:\\test.jpg"); //上传得本地文件
        OssUploadResult uploadResult = ossTemplate.upload(ossUploadRequest);//上传结果
        OssDownloadRequest downloadRequest = new OssDownloadRequest();
        downloadRequest.setType("local");  //采用本地
        downloadRequest.setBucketName("d:\\"); //在d盘中下载文件
        downloadRequest.setKey("test.png"); //文件名是test.png
        downloadRequest.setExtendParam(null);
        OssDownloadResult downloadResult = ossTemplate.download(downloadRequest);
        File file = new File("d:\\test.png"); //测试看下载得文件对不对
        FileUtil.createFile(file);
        FileUtil.writeByteArrayToFile(file, downloadResult.getByteArray());
        OssDeleteRequest deleteRequest = new OssDeleteRequest();
        deleteRequest.setType("local");//本地文件删除
        deleteRequest.setBucketName("d:\\");//删除本地d盘
        deleteRequest.setKey("test.png");//test.png文件
        deleteRequest.setExtendParam(null);
        OssDeleteResult deleteResult = ossTemplate.delete(deleteRequest);
        return Result.ok();
    }

    @GetMapping("/oss")
    public Result oss() throws IOException {
        OssUploadRequest ossUploadRequest = new OssUploadRequest();
        ossUploadRequest.setBucketName("");//对应存储空间名称
        ossUploadRequest.setType("aliyun"); //对应配置的key
        ossUploadRequest.setKey("");//对应上传的key
        ossUploadRequest.setStorageType(OssStorageType.FILE);
        ossUploadRequest.setFile(new File("d:\\test.jpg"));
        ossUploadRequest.setExtendParam(null);//扩展字段
        OssUploadResult uploadResult = ossTemplate.upload(ossUploadRequest);

        OssDownloadRequest downloadRequest = new OssDownloadRequest();
        downloadRequest.setBucketName("");
        downloadRequest.setKey("");
        downloadRequest.setType("aliyun");
        downloadRequest.setExtendParam(null);
        OssDownloadResult downloadResult = ossTemplate.download(downloadRequest);
        File file = new File("d:\\test.png");
        FileUtil.createFile(file);
        FileUtil.writeByteArrayToFile(file, downloadResult.getByteArray());
        OssDeleteRequest deleteRequest = new OssDeleteRequest();
        deleteRequest.setBucketName("");
        deleteRequest.setKey("");
        deleteRequest.setType("aliyun");
        deleteRequest.setExtendParam(null);
        OssDeleteResult deleteResult = ossTemplate.delete(deleteRequest);

        return Result.ok();
    }


    @EncryptMapping
    @GetMapping("/encrypt")
    public Result encrypt() {
        return Result.ok("123456");
    }

    @DecryptMapping
    @PostMapping("/decrypt")
    public Result decrypt(@RequestBody Result result) {
        return result;
    }

    @EncryptMapping("pacebox")
    @GetMapping("/pacebox/encrypt")
    public Result paceboxAjax() {
        return Result.ok("123456");
    }

    @DecryptMapping("pacebox")
    @PostMapping("/pacebox/decrypt")
    public Result paceboxDecrypt(@RequestBody Result result) {
        return result;
    }

    @EncryptMapping(type = EncryptType.FIELD)
    @DecryptMapping(type = DecryptType.FIELD)
    @PostMapping("/field/decrypt")
    public T1 fieldDecrypt(@RequestBody T1 t) {
        System.out.println(t.getId());
        return t;
    }


    @GetMapping("/ignoreTrace")
    public Result ignoreTrace() {

        sysUserService.getByUserName("1");

        RequestModel<String> requestModel = new RequestModel<>() {
        };
        requestModel.setMethod(HttpMethod.GET);
        requestModel.setUrl("http://127.0.0.1/open");
        ResponseEntity<String> result = singleRestTemplate.request(requestModel);
        if (result.getStatusCode() == HttpStatus.OK) {
            return Result.ok(result.getBody());
        }


        return Result.ok(1);
    }

    @GetMapping("/jaegerTestMethod")
    public Result jaegerTestMethod(String id) {
        RequestModel<String> requestModel = new RequestModel<>() {
        };
        requestModel.setMethod(HttpMethod.GET);
        requestModel.setUrl("http://127.0.0.1/open");
        ResponseEntity<String> result = singleRestTemplate.request(requestModel);
        if (result.getStatusCode() == HttpStatus.OK) {
            return Result.ok(result.getBody());
        }
        return Result.ok(id);
    }

    @GetMapping("/jaeger/test")
    public Result jaegerTest(String id) {
        sysUserService.getById("1");
        RequestModel<Result> requestModel = new RequestModel<>() {
        };
        requestModel.setMethod(HttpMethod.GET);
        requestModel.setUrl("http://127.0.0.1:8084/test/jaegerTestMethod?id=" + id);
        ResponseEntity<Result> result = singleRestTemplate.request(requestModel);
        if (result.getStatusCode() == HttpStatus.OK) {
            RequestModel<Result> requestModel2 = new RequestModel<>() {
            };
            requestModel2.setUrl("http://127.0.0.1:8084/test/jaeger/post");
            requestModel2.setParams(result.getBody());
            result = singleRestTemplate.request(requestModel2);
            if (result.getStatusCode() == HttpStatus.OK) {
                sysUserService.queryAll();
                return result.getBody();
            }
            return Result.faild("错误状态码" + result.getStatusCode().value() + "");
        }

        return Result.faild("错误状态码" + result.getStatusCode().value() + "");
    }

    @PostMapping("/jaeger/post")
    public Result post(@RequestBody Result result) {
        return result;
    }

    @Autowired(required = false)
    private Tracer tracer;





    @Autowired(required = false)
    private OkHttpClient.Builder traceOkHttpClientBuilder;

    @GetMapping("/testOkHttpClient")
    public Result testOkHttpClient() throws IOException {
        OkHttpClient httpClient = traceOkHttpClientBuilder.build();
        Request request = new Request.Builder().url("http://www.baidu.com").build();
        httpClient.newCall(request).execute();
        return Result.ok();
    }

    @PostMapping("/testClientReturn")
    public Result testClientReturn(@RequestBody String id) {
        return Result.ok(id);
    }

    @Data
    static class T1 {
        @DataSecureField
        private String id;

    }
}
