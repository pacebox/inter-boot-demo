package tech.mhuang.interchan.sso.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import tech.mhuang.pacebox.core.async.AsyncTaskService;

/**
 * @ClassName: RedisCacheStartup
 * @Author: Ever
 * @Description: Redis
 * @Date: 2019/12/28 0:11
 * @Version: 1.0
 */
@Component("redisCacheStartup")
@EnableAsync
public class RedisCacheStartup implements CommandLineRunner {

    @Autowired
    private AsyncTaskService asyncTaskService;

    @Autowired
    RedisCacheStartupService startupService;

    @Override
    public void run(String... args) {
        asyncTaskService.submit(startupService);
    }
}
