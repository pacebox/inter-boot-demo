package tech.mhuang.interchan.sso.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 {

    private static final Logger logger = LoggerFactory.getLogger(MD5.class);

    public static String GetMD5Code(String source, int length) {
        String result = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(source.getBytes());
            byte b[] = md.digest();
            int i;
            StringBuilder buf = new StringBuilder();
            for (byte value : b) {
                i = value;
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            result = buf.toString();
            if (length != 32) {
                result = buf.substring(8, 24);
            }
        } catch (NoSuchAlgorithmException e) {
            logger.error("MD5加密异常", e);
        }
        return result;
    }
}
