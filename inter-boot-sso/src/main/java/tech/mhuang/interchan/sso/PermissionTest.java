package tech.mhuang.interchan.sso;

import org.springframework.stereotype.Component;
import tech.mhuang.pacebox.springboot.core.local.GlobalHeaderThreadLocal;
import tech.mhuang.pacebox.springboot.core.permission.annotation.Permission;
import tech.mhuang.pacebox.springboot.core.permission.extra.IPermissionManager;

@Component("permissionTest")
public class PermissionTest implements IPermissionManager {

    @Override
    public void permission(Permission permissionUser) {
        //此处开放对接接口
        GlobalHeaderThreadLocal.get().getExtraMap().put("code", permissionUser.code());
    }
}
