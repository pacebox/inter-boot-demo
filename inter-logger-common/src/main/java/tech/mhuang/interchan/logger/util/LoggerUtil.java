package tech.mhuang.interchan.logger.util;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import tech.mhuang.interchan.logger.annoation.ESLogger;
import tech.mhuang.interchan.logger.consts.LoggerConsts;
import tech.mhuang.interchan.logger.entity.ESOperatorLogger;
import tech.mhuang.pacebox.core.date.DateTimeUtil;
import tech.mhuang.pacebox.core.util.StringUtil;
import tech.mhuang.pacebox.springboot.core.constans.Global;
import tech.mhuang.pacebox.springboot.core.spring.util.IpUtil;

import java.lang.reflect.Method;
import java.net.UnknownHostException;
import java.time.LocalDateTime;

/**
 * 日志工具类
 *
 * @author mhuang
 * @since 1.0.0
 */
@Slf4j
public class LoggerUtil {

    /**
     * 组装EsLogger数据
     *
     * @param request request
     * @return EsOperatorLogger EsOperatorLogger
     */
    public static ESOperatorLogger getEsLogger(HttpServletRequest request, JoinPoint jPoint) {
        ESOperatorLogger esLogger = new ESOperatorLogger();

        LocalDateTime now = LocalDateTime.now();
        //基础配置
        try {
            esLogger.setIp(IpUtil.getIp(request));
        } catch (UnknownHostException e) {
            log.error("日志:{}获取不到IP地址异常", request.getMethod(), e);
        }
        esLogger.setType(request.getMethod());
        esLogger.setUrl(request.getRequestURL().toString());
        esLogger.setStartDate(now + "+08:00");
        esLogger.setStartDateUnit(DateTimeUtil.localDateTimeToDate(now));
        //auth
        packAuthData(request, esLogger);

        Object[] args = jPoint.getArgs();
        //组装传输对象数组
        if (args != null && args.length > 0) {
            try {
                String data = JSON.toJSONString(args);
                data = data.replaceAll("\\\\", "");
                esLogger.setSendData(data);
            } catch (Exception ignored) {
            }
        }
        esLogger.setQueryData(request.getQueryString());
        packMethod(jPoint, esLogger);
        return esLogger;
    }

    public static ESOperatorLogger getEsLogger(JoinPoint jPoint) {
        ESOperatorLogger esLogger = new ESOperatorLogger();
        LocalDateTime now = LocalDateTime.now();
        //基础配置
        esLogger.setStartDate(now + "+08:00");
        esLogger.setStartDateUnit(DateTimeUtil.localDateTimeToDate(now));
        //auth
        Object[] args = jPoint.getArgs();
        //组装传输对象数组
        if (args != null && args.length > 0) {
            String data;
            try {
                data = JSON.toJSONString(args);
                data = data.replaceAll("\\\\", "");
                esLogger.setSendData(data);
            } catch (Exception ignored) {
            }
        }
        packMethod(jPoint, esLogger);
        return esLogger;
    }

    private static void packMethod(JoinPoint joinPoint, ESOperatorLogger esOperatorLogger) {
        String clazz = joinPoint.getTarget().getClass().getName();
        esOperatorLogger.setClazz(clazz);

        MethodSignature method = (MethodSignature) joinPoint.getSignature();
        esOperatorLogger.setMethod(method.getName());

        //注解
        try {
            String remark = getMethodByRemark(method);
            esOperatorLogger.setRemark(remark);
        } catch (Exception ignored) {
        }
    }

    public static void packAuthData(HttpServletRequest request, ESOperatorLogger esLogger) {
        try{
            if (LoggerConsts.getJwtHeader()) {
                String header = request.getHeader(LoggerConsts.getJwtHeaderName());
                if(StringUtil.isNotEmpty(header)){
                    esLogger.setHeaderData(header.replaceAll("\\\\", ""));
                }
                if (StringUtil.isNotBlank(esLogger.getHeaderData())) {
                    JSONObject obj = JSON.parseObject(esLogger.getHeaderData());
                    esLogger.setUserId(obj.getString(Global.USER_ID));
                }
            }
        }catch(Exception e){
            log.error("获取权限失败",e);
        }
    }

    public static String getMethodByRemark(MethodSignature methodSignature) throws Exception {
        Method method0 = methodSignature.getMethod();
        ESLogger esLogger = method0.getAnnotation(ESLogger.class);
        if (esLogger != null) {
            return esLogger.remark();
        }
        return null;
    }
}