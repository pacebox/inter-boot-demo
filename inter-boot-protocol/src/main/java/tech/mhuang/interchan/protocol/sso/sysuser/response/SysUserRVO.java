
package tech.mhuang.interchan.protocol.sso.sysuser.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @ClassName: SysUserPageQryRVO
 * @author: admin
 * @date: 2018年3月23日 上午10:09:37
 */
@Data
public class SysUserRVO {
    @Schema(description = "userId")
    private String userid;
    /**
     * 用户名称
     */
    @Schema(description = "用户名")
    private String username;

    /**
     * 密码
     */
    @Schema(description = "密码")
    private String password;

    /**
     * Email
     */
    @Schema(description = "邮箱")
    private String email;

    /**
     * 电话
     */
    @Schema(description = "电话")
    private String tel;

    /**
     * 移动电话
     */
    @Schema(description = "移动电话")
    private String mobilephone;

    /**
     * 用户账号是否有效[1=有效(default)})/0=无效]
     */
    @Schema(description = "锁定")
    private String available;
    /**
     * 地址
     */
    @Schema(description = "地址")
    private String address;
}
