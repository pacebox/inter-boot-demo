package tech.mhuang.interchan.protocol.sso.sysuser.pwd;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 修改个人中心密码
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UpdateCenterPwdDTO {

    @Schema(description = "旧密码")
    private String oldPassword;

    /**
     * 密码
     */
    @Schema(description = "新密码")
    private String password;

    /**
     * 用户id
     */
    @Schema(description = "用户id")
    private String userid;

    /**
     * 操作的用户id
     */
    @Schema(description = "操作用户id", hidden = true)
    private String operateUser;
}
