package tech.mhuang.interchan.protocol.sso;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserDTO {

    /**
     * 用户id
     */
    @Schema(description = "用户id")
    private String userid;

    /**
     * 操作的用户id
     */
    @Schema(description = "操作用户id", hidden = true)
    private String operateUser;

}
