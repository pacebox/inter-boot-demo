package tech.mhuang.interchan.protocol.sso.sysuser.pwd;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 修改密码
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UpdatePwdDTO {


    /**
     * 密码
     */
    @Schema(description = "密码")
    private String password;

    /**
     * 用户id
     */
    @Schema(description = "用户id")
    private String userid;
}
