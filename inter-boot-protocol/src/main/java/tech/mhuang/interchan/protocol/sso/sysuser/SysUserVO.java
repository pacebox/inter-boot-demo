package tech.mhuang.interchan.protocol.sso.sysuser;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class SysUserVO {

    private String userid;
    /**
     * 用户名称
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * Email
     */
    private String email;

    /**
     * 电话
     */
    private String tel;

    /**
     * 移动电话
     */
    private String mobilephone;

    /**
     * 用户账号是否有效[1=有效(default)})/0=无效]
     */
    private String available;
    /**
     * 地址
     */
    private String address;

    /**
     * 账号
     */
    private String account;
    /**
     * 是否是管理员 0 是 1 不是
     */
    private String manager;
    /**
     * 关联的公司id
     */
    private String companyId;

    private String companyName;
}
