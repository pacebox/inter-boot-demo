package tech.mhuang.interchan.protocol.sso.sysuser;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class LoginSysUserVO {

    /**
     * 用户名称
     */
    @Schema(description = "用户名")
    private String username;


    /**
     * 用户ID
     */
    @Schema(description = "用户ID")
    private String userid;


    /**
     * 密码
     */
    @Schema(description =  "密码")
    private String password;

    /**
     * Email
     */
    @Schema(description = "邮箱")
    private String email;

    /**
     * 电话
     */
    @Schema(description = "电话")
    private String tel;

    /**
     * 移动电话
     */
    @Schema(description = "移动电话")
    private String mobilephone;

    /**
     * 地址
     */
    @Schema(description = "地址")
    private String address;


    /**
     * 令牌
     */
    @Schema(description = "令牌")
    private String token;

    /**
     * 临时
     */
    private String authType;

    /**
     * 用户登陆时是否要求修改密码
     */
    @Schema(description = "用户登陆时是否要求修改密码")
    private String chgpwdflag;

    /**
     * 是否是管理员 0 是 1 不是
     */
    private String manager;
    /**
     * 关联的公司id
     */
    private String companyId;

    private String companyName;

}
