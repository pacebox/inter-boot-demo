package tech.mhuang.interchan.protocol.sso.sysuser.pwd;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ResetPwdDTO {

    /**
     * 密码
     */
    @Schema(description = "密码")
    private String password;

    /**
     * 用户id
     */
    @Schema(description = "用户id")
    private String userid;

    /**
     * 操作的用户id
     */
    @Schema(description = "操作用户id", hidden = true)
    private String operateUser;
}
