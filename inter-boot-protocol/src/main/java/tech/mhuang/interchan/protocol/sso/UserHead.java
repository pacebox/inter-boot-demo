package tech.mhuang.interchan.protocol.sso;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户头像
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserHead {

    @Schema(hidden = true)
    private String userId;

    @Schema(description = "用户头像Id")
    private String attachId;
}
